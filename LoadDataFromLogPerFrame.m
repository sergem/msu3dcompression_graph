function [  ] = LoadDataFromLogPerFrame( path, regexName, isSSIM )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


marker = [ 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*'];
color =  [ 'b' 'g' 'r' 'c' 'm' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'b' 'g' 'r' 'c' 'm' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'b' 'g' 'r' 'c' 'm' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y'];

figure1 = figure('Position',[50 50 800 700]);
axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','LineWidth',1,...
    'XMinorTick','on',...
    'XMinorGrid','on');  %,'XScale','log'


box(axes1,'on');
hold(axes1,'all');

xlabel('Number of frame');
if( isSSIM )
    ylabel('Y-YUV SSIM');
else
   ylabel('Y-YUV PSNR, dB');
end;
title( sprintf( 'Sequence %s', regexName ) );    



fid = fopen(path);
tline = fgets(fid);



count = 1;

while ischar(tline)
    %disp(tline)
    strregex = strcat( '(', regexName, '.*)size.*psnrPerFrame; \[(.*)\]; ssimPerFrame; \[(.*)\].*\n');
    tokens = regexp(tline, strregex, 'tokens'); 
    if( size(tokens) == [ 1 1 ] )
        curline = tokens{1,1};
        lineName = strcat(curline{1, 1},',');
        linePSNR = strcat(curline{1, 2},',');
        lineSSIM = strcat(curline{1, 3},',');
        
        lineName = strrep(lineName,'\\','_');
        
        valuesPSNR = regexp(linePSNR,',','split');
        valuesSSIM = regexp(lineSSIM,',','split');
        
        len = size(valuesPSNR,2);
        psnr = zeros(1,len);
        ssim = zeros(1,len);
        
        
        for i= 1:len
            psnr(i) = str2double(valuesPSNR{i});
            ssim(i) = str2double(valuesSSIM{i}); 
        end;
       
        vls = [];
        if( isSSIM )
            vls = ssim;
        else
            vls = psnr;
        end;
            
        %plot( 1:len, ssim );
        
        
        plot( 0:len-1, vls, 'MarkerFaceColor', color(count),'Marker',marker(count), 'DisplayName', lineName,  'LineWidth',2,  'Color', color(count) );
        %count = int16(mod( count, 15 )) + 1;
        count = count + 1;
       
%         disp (curline)
    end;
    
    tline = fgets(fid);
end;

fclose(fid);
legend1 = legend(axes1,'show');
set(legend1,'Interpreter','none','Location','Best');

end

