function [ arraySize, arrayPSNR, arraySSIM ] = LoadDataFromLog( path, regexName )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
fid = fopen(path);
arraysize = [];
arraypsnr = [];
arrayssim = [];
tline = fgets(fid);
while ischar(tline)
    %disp(tline)
    strregex = strcat( regexName, '; size; ([0-9.]+); PSNR; ([0-9.]+); SSIM; ([0-9.]+); .*\n');
    tokens = regexp(tline, strregex, 'tokens'); 
    if( size(tokens) == [ 1 1 ] )
        curline = tokens{1,1};
        sz = str2double(curline{1, 1});
        psnr = str2double(curline{1, 2});
        ssim = str2double(curline{1, 3});
        arraysize = [ arraysize; sz ];
        arraypsnr = [ arraypsnr; psnr ];
        arrayssim = [ arrayssim; ssim ];
%         disp (curline)
    end
    
    tline = fgets(fid);
end

fclose(fid);
arraySize =arraysize;
arrayPSNR = arraypsnr;
arraySSIM = arrayssim;

end

