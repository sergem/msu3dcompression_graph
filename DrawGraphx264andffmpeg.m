function [ ] = DrawGraphx264andffmpeg( path, regexNameBegin, w, h, numFrames )
%DRAWGRAPH Summary of this function goes here
%   Detailed explanation goes here

    scale = [1 2 4];
    quality = [ 64 128 256 512 ];
    marker = [ 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' ];
    color = [ 'b' 'g' 'r' 'c' 'm' 'y' 'w' 'k' ];
    A = [];
    B = [];
    C = [];
    D = [];
        


figure1 = figure;
    axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','LineWidth',1);  %,'XScale','log'
    box(axes1,'on');
    hold(axes1,'all');

    xlabel('Bitrate, bits per pixel');
    ylabel('Y-YUV PSNR, dB');
    title('Sequence Bovik1');
    

    %for i = 1:count,    
        
    %end;
    
    

    i = 1;
    
    
    
    
    for sc = 1:length(scale),
        for q = 1:length(quality),
            if ( quality( q ) == 512 && scale (sc) == 4 )
                %continue;
            end;
            %i = floor( i );
            [A{i} B{i} C{i}] = LoadDataFromLog( path, sprintf( '%s_.*scale%d.*q%d.*', regexNameBegin, scale(sc), quality(q) ) );
            %disp( A{i} )
            A{i} = A{i} / ( w * h * numFrames ) * 8;
            D{i} = sprintf( 'Downscale=%d, quality=%d', scale(sc), quality(q) );
            plot( A{i}, B{i}, 'MarkerFaceColor', color(sc),'Marker',marker(q), 'DisplayName', D{i},  'LineWidth',2,  'Color', color(sc) );
            i = 1 + i;
        end,
        
    end;
    
    color = [  'c' 'm' 'y' 'w' 'k' ];
    for sc = 1:length(scale),
        for q = 1:length(quality),
            if ( quality( q ) == 512 && scale (sc) == 4 )
                %continue;
            end;
            %i = floor( i );
            [A{i} B{i} C{i}] = LoadDataFromLog( path, sprintf( '%sffmpeg.*scale%d.*q%d.*', regexNameBegin, scale(sc), quality(q) ) );
            %disp( A{i} )
            A{i} = A{i} / ( w * h * numFrames ) * 8;
            D{i} = sprintf( 'OLD Downscale=%d, quality=%d', scale(sc), quality(q) );
            plot( A{i}, B{i}, 'MarkerFaceColor', color(sc),'Marker',marker(q), 'DisplayName', D{i},  'LineWidth',2,  'Color', color(sc) );
            i = 1 + i;
        end,
        
    end;
    
    
    color = [ 'b' 'g' 'r' 'c' 'm' 'y' 'w' 'k' ];
    sc = 1;
    q = 1;    
       
    [A{i} B{i} C{i}] = LoadDataFromLog( path, sprintf( '%s.*x264_.*', regexNameBegin ) );
    A{i} = A{i} / ( w * h * numFrames ) * 8;
    D{i} = sprintf( 'x264' );
    plot( A{i}, B{i}, 'MarkerFaceColor', color(4),'Marker',marker(q), 'DisplayName', D{i},  'LineWidth',2,  'Color', color(4) );
    i = 1 + i;
    
       
    [A{i} B{i} C{i}] = LoadDataFromLog( path, sprintf( '.*%s.*x264ff.*', regexNameBegin ) );
    A{i} = A{i} / ( w * h * numFrames ) * 8;
    D{i} = sprintf( 'x264 ffmpeg' );
    plot( A{i}, B{i}, 'MarkerFaceColor', color(5),'Marker',marker(7), 'DisplayName', D{i},  'LineWidth',2,  'Color', color(5) );
    i = 1 + i;
    
    
    count = i - 1;
  
    
    
    legend(axes1,'show', 'Location','Best');
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %     commandPlotPSNR = 'plot( A{1}, B{1}';
    %     for i = 2:length(scale),
    %         commandPlotPSNR = strcat( commandPlotPSNR, sprintf( ', A{%d}, B{%d}', i, i ) ); 
    %     end;
    %     commandPlotPSNR = strcat( commandPlotPSNR, ');' );
    %     eval(commandPlotPSNR);

end

