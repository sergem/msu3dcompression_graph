function [ ] = DrawGraphx264( path, regexNameBegin, w, h, numFrames, titleS, isSSIM, thNumPixToTrimX, thNumPixToTrimY )
%DRAWGRAPH graphs for english article 
%   Detailed explanation goes here

    if nargin < 8
        thNumPixToTrimX =  0;
    end;

    if nargin < 9
        thNumPixToTrimY =  0;
    end;


    width = 20;         % Initialize a variable for width.
    height = 18;          % Initialize a variable for height.
    bleft = 1;
    bright = 0.1;
    btop = 2;
    bbottom = 1;
    widthext = width + bleft + bright;
    heightext = height + btop + bbottom;

    scale = [1 2 4 8 16];
    quality = [ 16 32 64 128 256 512 768 1024 ];
    marker = [ 's' 'o' '^' '+' '*' 'x' 'd'  'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '^' '+' '*' 'x' 'd'  'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '^' '+' '*' 'x' 'd'  'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h'];
    %color = [ 'b' 'g' 'r' 'm' 'k' 'b' 'c' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' ];
    color = [ ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        ];
    
 
    
    valuesYAll = [];
    valuesXAll = [];
    A = [];
    B = [];
    C = [];
    D = [];
        

    
    scrsz = get(0,'ScreenSize');
    
    figure1 = figure( ... %'Position',[50 50 1300 800], ...%'PaperSize',[112000 111800],... %
        'Color',[1 1 1]);
    axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','LineWidth',1,...
        'XScale','linear',...  
        'FontSize',14 );  
    set(axes1,'Units', 'centimeters' );
    set(axes1,'ActivePositionProperty', 'OuterPosition' );
    %set(axes1,'Position',[0 1 widthext height] );
    set(axes1,'OuterPosition',[0 0 width height] );
    %, 'Position',[0.06 0.088 0.92 0.86]
    %, 'Position',[0.1 0.1 0.9 0.9]
    box(axes1,'on');
    hold(axes1,'all');
    
    

    xlabel('Bitrate, bits per pixel', ...
        'FontSize',14);
    if( isSSIM )
        ylabel('Y-YUV SSIM of stereo', ...
        'FontSize',14);
    else
        ylabel('Y-YUV PSNR of depth map, dB', ...
        'FontSize',14);
    end;
    
    %title( titleS, ...
    %    'FontSize',14 );    
    
    
  
    i = 1;
    
    
    for sc = 1:length(scale),
        for q = 1:length(quality),
            if ( quality( q ) == 512 && scale (sc) == 4 )
                continue;
            end;
            
            DrawOneLine( path, ...
                sprintf( '%s.*vetrov.*scalev%d.*q%d.*', regexNameBegin, scale(sc), quality(q) ), ...
                sprintf( 'Proposed+FFmpeg, k=%d, q=%3d', scale(sc), quality(q) ), ...
                isSSIM, sc, q, w, h, numFrames, ':' );
        
    

        end,
        
    end;
    
    for sc = 1:length(scale),
        for q = 1:length(quality),
            if ( quality( q ) == 512 && scale (sc) == 4 )
                %continue;
            end;
            
            st = '-';
            if ( scale(sc) == 4 )
               %st = ':';
            end;
              
            
            DrawOneLine( path, ...
                sprintf( '%s.*scale%d_.*q%d.*', regexNameBegin, scale(sc), quality(q) ), ...
                sprintf( 'Proposed+ImageMagick, k=%d, q=%3d', scale(sc), quality(q) ), ...
                isSSIM, sc, q, w, h, numFrames, st );
        
    
            
        end,
        
    end;
    
    
    
    i = 5;
    

    DrawOneLine( path, ...
            sprintf( '.*%s.*_manual_.*', regexNameBegin ), ...
            sprintf( 'Proposed+adaptive key frames' ), ...
            isSSIM, i, i, w, h, numFrames, '-' );
    i = 1 + i;
    
    
    DrawOneLine( path, ...
            sprintf( '.*%s.*x264_.*', regexNameBegin ), ...
            sprintf( 'x264'  ), ...
            isSSIM, i, i, w, h, numFrames, '-' );
    i = 1 + i;
    
    DrawOneLine( path, ...
            sprintf( '.*%s.*vetrov.*h264.*', regexNameBegin ), ...
            sprintf( 'x264 psychovisual optimization'  ), ...
            isSSIM, i, i, w, h, numFrames, ':' );
    i = 1 + i;
    
    DrawOneLine( path, ...
            sprintf( '.*%s.*var_step.*', regexNameBegin ), ...
            sprintf( 'Variable step Iframes' ), ...
            isSSIM, i, i, w, h, numFrames, '-' );
    i = 1 + i;
    
    
    DrawOneLine( path, ...
            sprintf( '.*%s.*_du_.*', regexNameBegin ), ...
            sprintf( 'DU' ), ...
            isSSIM, i, i, w, h, numFrames, '-' );
    i = 1 + i;
    
    
    for q = 1:length(quality),
        DrawOneLine( path, ...
            sprintf( '.*%s.*pframes.*_q%d.*', regexNameBegin, quality(q) ), ...
            sprintf( 'Variable step I-, P-frames q=%d', quality(q) ), ...
            isSSIM, i, i, w, h, numFrames, '-' );
        i = 1 + i;
    end;
    

    
    count = i - 1;
  
    
    
    
    %set(gcf, 'PaperUnits', 'centimeters')
    
    papersize = [widthext, heightext];
    set(gcf, 'PaperSize', papersize );

    %left = (papersize(1)- width)/2;
    %bottom = (papersize(2)- height)/2;

    myfiguresize = [bleft, bbottom, width, height];
    set(gcf, 'PaperPosition', myfiguresize);
    
    labelName = 'psnr';
    if( isSSIM )
        labelName = 'ssim';
    end;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    if( thNumPixToTrimY > 0 )
        valuesYAll = sort( valuesYAll );
        
        min = valuesYAll(thNumPixToTrimY);
        max = valuesYAll(length(valuesYAll)-thNumPixToTrimY);
        range = max - min;
        min = min - range * 0.05;
        max = max + range * 0.05;
        ylim('manual');
        ylim( [min max] );
    end;
    
    if( thNumPixToTrimX > 0 )
        valuesXAll = sort( valuesXAll );
        
        min = valuesXAll(thNumPixToTrimX);
        max = valuesXAll(length(valuesXAll)-thNumPixToTrimX);
        range = max - min;
        min = min - range * 0.05;
        max = max + range * 0.05;
        xlim('manual');
        xlim( [min max] );
        
    end;
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    legend1 = legend(axes1,'show');
    set(legend1,'Interpreter','none','Location','SouthEast',...
        'FontWeight','bold',...
        'FontSize',12 );
    set( legend1,'FontName','Courier New' );
    set( legend1,'Location','SouthEast' );
    titleA = sprintf('%s', titleS{:});
    pathBase = strrep( strrep( titleA, '"', '' ), '\n', '' );
    print(gcf,'-dtiff', '-r100', sprintf( '%s%s.eng.100.tiff', pathBase, labelName ));
    print(gcf,'-dtiff', '-r300', sprintf( '%s%s.eng.300.tiff', pathBase, labelName ));
    saveas(gcf, sprintf( '%s%s.eng.fig', pathBase, labelName ))
    %print -djpeg -f2 -r300 myfigure
    %frame=getframe(gcf);
    %[X,map]=frame2im(frame);
    %imwrite(X,sprintf( '%s%s.png', titleS, labelName ) );
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %     commandPlotPSNR = 'plot( A{1}, B{1}';
    %     for i = 2:length(scale),
    %         commandPlotPSNR = strcat( commandPlotPSNR, sprintf( ', A{%d}, B{%d}', i, i ) ); 
    %     end;
    %     commandPlotPSNR = strcat( commandPlotPSNR, ');' );
    %     eval(commandPlotPSNR);
    
    function [numLinesAdded] = DrawOneLine( path, regexp, caption, isSSIM, idxColor, idxMarker, w, h, numFrames, styleOfLine )
        A = ReadMeasurementsData( path, 'size', regexp );
        B = ReadMeasurementsData( path, 'PSNR', regexp );
        C = ReadMeasurementsData( path, 'SSIM', regexp );
        A = A / ( w * h * numFrames ) * 8;

        vls = [];
        if( isSSIM )
            vls = C;
        else
            vls = B;
        end;
        valuesYAll = [ valuesYAll; vls ];
        valuesXAll = [ valuesXAll; A ];
        plot( A, vls, ...
            'MarkerFaceColor', color(idxColor,:), ...
            'Marker',marker(idxMarker), ...
            'DisplayName', caption, ...
            'LineWidth',1, ...
            'Color', color(idxColor, :), ...
            'LineStyle', styleOfLine );
        if size(A) == 0
            numLinesAdded = 0;
        else
            numLinesAdded = 1;
        end;
    end    

end



