DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/parrots.txt', 'parrots', 1920, 1080, 22, { 'Sequence "Parrots". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/parrots.txt', 'parrots', 1920, 1080, 22, { 'Sequence "Parrots". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/bovic.txt', 'bovic1', 960, 1080, 450, { 'Sequence "Bovik1". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/bovic.txt', 'bovic1', 960, 1080, 450, { 'Sequence "Bovik1". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/bovic.txt', 'bovic2', 960, 1080, 450, { 'Sequence "Bovik2". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/bovic.txt', 'bovic2', 960, 1080, 450, { 'Sequence "Bovik2". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/bovic.txt', 'bovic3', 960, 1080, 450, { 'Sequence "Bovik3". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/bovic.txt', 'bovic3', 960, 1080, 450, { 'Sequence "Bovik3". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/gargoyle.txt', 'gargoyle', 1000, 430, 30, { 'Sequence "Gargoyle". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/gargoyle.txt', 'gargoyle', 1000, 430, 30, { 'Sequence "Gargoyle". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/people.txt', 'people', 1920, 1080, 33, { 'Sequence "People". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/people.txt', 'people', 1920, 1080, 33, { 'Sequence "People". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/cloudy.txt', 'cloudy', 960, 824, 516, { 'Sequence "Cloudy". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/cloudy.txt', 'cloudy', 960, 824, 516, { 'Sequence "Cloudy". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/ira.txt', 'ira', 1920, 1080, 1929, { 'Sequence "Ira". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/ira.txt', 'ira', 1920, 1080, 1929, { 'Sequence "Ira". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );

DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/pirates.txt', 'pirates_.*', 960, 400, 750, { 'Sequence "Pirates". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0 );
DrawGraphx264( 'logs20120505_no_outliers_no_ffmpeg/pirates.txt', 'pirates_.*', 960, 400, 750, { 'Sequence "Pirates". Compression using the proposed scheme with constant intervals between depth key frames.' }, 1 );