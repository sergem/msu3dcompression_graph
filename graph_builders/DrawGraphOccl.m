function [ ] = DrawGraphOccl( path, arrayPrefix, arrayLabels, listScale, listQuality, numFrames, titleS, isSSIM, thNumPixToTrimX, thNumPixToTrimY )
%DrawGraphOccl( path, arrayPrefix, arrayLabels, listScale, listQuality, w, h, numFrames, titleS, isSSIM, thNumPixToTrimX, thNumPixToTrimY ) 
%  Example: DrawGraphOccl( 'logs_statue_adv_vs_normal/statue_all.txt', { 'new', 'dp' }, { 'Occlusion processing on', 'Occlusion processing off' }, 2, 256 , 100, { 'Sequence "Statue". Compression using the proposed scheme with constant intervals between depth key frames.' }, 0, 1, 1 );

    if nargin < 9
        thNumPixToTrimX =  0;
    end;

    if nargin < 10
        thNumPixToTrimY =  0;
    end;


    width = 20;         % Initialize a variable for width.
    height = 18;          % Initialize a variable for height.
    bleft = 1;
    bright = 0.1;
    btop = 2;
    bbottom = 1;
    widthext = width + bleft + bright;
    heightext = height + btop + bbottom;

    scale = [1 2 4 8 16];
    quality = [ 16 32 64 128 256 ];
    marker = [ 's' 'o' '^' '+' '*' 'x' 'd'  'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '^' '+' '*' 'x' 'd'  'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '^' '+' '*' 'x' 'd'  'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h' 's' 'o' '+' '*' 'x' 'd' '^' 'v' '>' '<' 'p' 'h'];
    %color = [ 'b' 'g' 'r' 'm' 'k' 'b' 'c' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' 'b' 'g' 'r' 'c' 'm' 'y' 'k' ];
    color = [ ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7]; [153 102 0]/256; ...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        [0 0 1]; [0 0.7 0]; [1 0 0]; [0 0.8 0.8]; [0 0 0]; [0.7 0.7 0]; [0.7 0 0.7];...
        ];
    
 
    
    valuesYAll = [];
    valuesXAll = [];
    A = [];
    B = [];
    C = [];
    D = [];
        

    
    scrsz = get(0,'ScreenSize');
    
    figure1 = figure( ... %'Position',[50 50 1300 800], ...%'PaperSize',[112000 111800],... %
        'Color',[1 1 1]);
    axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on','LineWidth',1,...
        'XScale','linear',...  
        'FontSize',14 );  
    set(axes1,'Units', 'centimeters' );
    set(axes1,'ActivePositionProperty', 'OuterPosition' );
    %set(axes1,'Position',[0 1 widthext height] );
    set(axes1,'OuterPosition',[0 0 width height] );
    %, 'Position',[0.06 0.088 0.92 0.86]
    %, 'Position',[0.1 0.1 0.9 0.9]
    box(axes1,'on');
    hold(axes1,'all');
    
    

    xlabel('Bitrate, kbps', ...
        'FontSize',14);
    if( isSSIM )
        ylabel('Y-YUV SSIM of stereo', ...
        'FontSize',14);
    else
        ylabel('Y-YUV PSNR of depth map, dB', ...
        'FontSize',14);
    end;
    
    %title( titleS, ...
    %    'FontSize',14 );    
    
    
  
    for iPrefix = 1:size( arrayPrefix, 2 )
    
        for iScale = 1:length(listScale),
            for iQaulity = 1:length(listQuality),
                st = '-';
                DrawOneLineBitrate( path, ...
                    sprintf( '%s.*scale%d_.*q%d.*', arrayPrefix{iPrefix}, listScale(iScale), listQuality(iQaulity) ), ...
                    arrayLabels{iPrefix}, ...
                    isSSIM, iPrefix, iPrefix, numFrames, st );
                %sprintf( 'Proposed+ImageMagick, k=%d, q=%3d', listScale(iScale), listQuality(iQaulity) ), ...
                    


            end,

        end;
    end;
    
   
    
    
  
    
    count = i - 1;
  
    
    
    
    %set(gcf, 'PaperUnits', 'centimeters')
    
    papersize = [widthext, heightext];
    set(gcf, 'PaperSize', papersize );

    %left = (papersize(1)- width)/2;
    %bottom = (papersize(2)- height)/2;

    myfiguresize = [bleft, bbottom, width, height];
    set(gcf, 'PaperPosition', myfiguresize);
    
    labelName = 'psnr';
    if( isSSIM )
        labelName = 'ssim';
    end;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    if( thNumPixToTrimY > 0 )
        valuesYAll = sort( valuesYAll );
        
        min = valuesYAll(thNumPixToTrimY);
        max = valuesYAll(length(valuesYAll)-thNumPixToTrimY);
        range = max - min;
        min = min - range * 0.05;
        max = max + range * 0.05;
        ylim('manual');
        ylim( [min max] );
    end;
    
    if( thNumPixToTrimX > 0 )
        valuesXAll = sort( valuesXAll );
        
        min = valuesXAll(thNumPixToTrimX);
        max = valuesXAll(length(valuesXAll)-thNumPixToTrimX);
        range = max - min;
        min = min - range * 0.05;
        max = max + range * 0.05;
        xlim('manual');
        xlim( [min max] );
        
    end;
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    
    legend1 = legend(axes1,'show');
    set(legend1,'Interpreter','none','Location','SouthEast',...
        'FontWeight','bold',...
        'FontSize',12 );
    set( legend1,'FontName','Courier New' );
    set( legend1,'Location','SouthEast' );
    titleA = sprintf('%s', titleS{:});
    pathBase = strrep( strrep( titleA, '"', '' ), '\n', '' );
    print(gcf,'-dtiff', '-r100', sprintf( '%s%s.eng.100.tiff', pathBase, labelName ));
    print(gcf,'-dtiff', '-r300', sprintf( '%s%s.eng.300.tiff', pathBase, labelName ));
    saveas(gcf, sprintf( '%s%s.eng.fig', pathBase, labelName ))
    %print -djpeg -f2 -r300 myfigure
    %frame=getframe(gcf);
    %[X,map]=frame2im(frame);
    %imwrite(X,sprintf( '%s%s.png', titleS, labelName ) );
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %     commandPlotPSNR = 'plot( A{1}, B{1}';
    %     for i = 2:length(scale),
    %         commandPlotPSNR = strcat( commandPlotPSNR, sprintf( ', A{%d}, B{%d}', i, i ) ); 
    %     end;
    %     commandPlotPSNR = strcat( commandPlotPSNR, ');' );
    %     eval(commandPlotPSNR);
    
    function [numLinesAdded] = DrawOneLineBitrate( path, regexp, caption, isSSIM, idxColor, idxMarker, numFrames, styleOfLine )
        A = ReadMeasurementsData( path, 'size', regexp );
        B = ReadMeasurementsData( path, 'PSNR', regexp );
        %C = ReadMeasurementsData( path, 'SSIM', regexp );
        fps = 30;
        A = A / ( numFrames ) * fps * 8 / 1000;

        vls = [];
        if( isSSIM )
            vls = C;
        else
            vls = B;
        end;
        valuesYAll = [ valuesYAll; vls ];
        valuesXAll = [ valuesXAll; A ];
        plot( A, vls, ...
            'MarkerFaceColor', color(idxColor,:), ...
            'Marker',marker(idxMarker), ...
            'DisplayName', caption, ...
            'LineWidth',1, ...
            'Color', color(idxColor, :), ...
            'LineStyle', styleOfLine );
        if size(A) == 0
            numLinesAdded = 0;
        else
            numLinesAdded = 1;
        end;
    end    

end



