function [ columnSize ] = ReadMeasurementsData( path, columnName, template, verbose )
% Usage: ReadMeasurementsData( path, columnName, template, verbose ) or
%   ReadMeasurementsData( path, columnName, template )
% Example:  ReadMeasurementsData( 'all.txt', 'PSNR', '.*dp' ) - loads all
%   data that has substring 'dp' in name (any predecessing characters are
%   not important)
    
    if nargin == 3, verbose = false;
    elseif nargin == 4, 1;
    else 
        error( 'Wrong number of arguments' );
    end;
    
    
    fid = fopen( path );

    res = [];
    tline = fgetl(fid);
    while ischar(tline)
        %disp(tline)
        parsed = regexp(tline, ';\s*', 'split' );
        if( size( res, 2 ) ~= 0 && size( parsed, 2 ) ~= size( res, 2 ) )
            if( verbose ), disp( [ 'Skipped line: "' tline '"' ] ); end;
        else
            curname =  parsed( 1 );
            match = regexp( curname, template, 'start', 'once' );
            
            if( isempty( match{1} ) || match{1}(1) ~= 1  )
                if( verbose ), disp( [ 'Skipped by name: "' tline '"' ] ); end;
            else
                if( verbose ), disp( [ 'Line loaded: "' tline '"' ] ); end;
                res = [ res; parsed ];
            end;
        end;
        tline = fgetl(fid);

    end;
    fclose(fid);

    if( isempty( res ) )
        columnSize = [];
    else
        indSize=find(ismember(res( 1, : ), columnName));
        if isempty( indSize ) || indSize + 1 > size( res, 2 ) 
            error( ['Column "' columnName '" not found'] );
        end;
        indSize = indSize + 1;

        columnSize = res( :, indSize );


        columnSize = cellfun(@str2num, columnSize );
    end;
end
